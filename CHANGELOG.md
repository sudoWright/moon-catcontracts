# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2022-02-23
### Added
- JumpPort Javascript interfaces

## [1.0.4] - 2022-09-21
### Fixed
- Add README to deployed package, as a copy of the README in the root of the repository.

## [1.0.3] - 2022-09-21
### Added
- Initialized repository as NPM module, and stabilized publishing infrastructure for it.
- Added Solidity interface files for existing MoonCat-related contracts
- Added Hardhat configuration to move `artifacts` folder inside the `contracts` folder, such that they could be published/bundled together
