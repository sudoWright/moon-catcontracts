const { network, ethers } = require('hardhat'),
  { expect } = require("chai");

const MOONCATRESCUE_ADDRESS = '0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6',
  MOONCAT_WRAPPED_ADDRESS = '0x7C40c393DC0f283F318791d746d894DdD3693572',
  MOONCAT_ACCLIMATOR_ADDRESS = '0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69',
  MOONCAT_ACCESSORIES_ADDRESS = '0x8d33303023723dE93b213da4EB53bE890e747C63',
  MOONCAT_LOOTPRINTS_ADDRESS = '0x1e9385eE28c5C7d33F3472f732Fb08CE3ceBce1F';
  JUMPPORT_ADDRESS = '0xF4d150F3D03Fa1912aad168050694f0fA0e44532';

const MoonCatRescueArtifact = require('./artifacts/contracts/IMoonCatRescue.sol/IMoonCatRescue.json'),
  OldWrappedMoonCatArtifact = require('./artifacts/contracts/IMoonCatsWrapped.sol/IMoonCatsWrapped.json'),
  AcclimatorArtifact = require('./artifacts/contracts/IMoonCatAcclimator.sol/IMoonCatAcclimator.json'),
  AccessoriesArtifact = require('./artifacts/contracts/IMoonCatAccessories.sol/IMoonCatAccessories.json'),
  LootprintsArtifact = require('./artifacts/contracts/IMoonCatLootprints.sol/IMoonCatLootprints.json');
  JumpPortArtifact = require('./artifacts/contracts/IJumpPort.sol/IJumpPort.json');

const  MoonCatRescue = ethers.getContractAt(MoonCatRescueArtifact.abi, MOONCATRESCUE_ADDRESS),
  OldWrappedMoonCats = ethers.getContractAt(OldWrappedMoonCatArtifact.abi, MOONCAT_WRAPPED_ADDRESS),
  MoonCatAcclimator = ethers.getContractAt(AcclimatorArtifact.abi, MOONCAT_ACCLIMATOR_ADDRESS),
  MoonCatAccessories = ethers.getContractAt(AccessoriesArtifact.abi, MOONCAT_ACCESSORIES_ADDRESS),
  MoonCatLootprints = ethers.getContractAt(LootprintsArtifact.abi, MOONCAT_LOOTPRINTS_ADDRESS),
  JumpPort = ethers.getContractAt(JumpPortArtifact.abi, JUMPPORT_ADDRESS);


async function mineUntilGasTarget(targetGas) {
  for(let i = 1; i <= 50; i++) {
    let blockData = await ethers.provider.getBlock('latest');
    if (typeof blockData.baseFeePerGas == 'undefined' || blockData.baseFeePerGas.lte(targetGas)) return;
    //console.log(blockData.number.toLocaleString(), ethers.utils.formatUnits(blockData.baseFeePerGas, 'gwei'));
    await network.provider.request({
      method: "evm_mine",
      params: []
    });
  }
}

async function disperseMoonCats(startIndex, maxIndex, numAccounts = 10) {
  const accounts = await ethers.getSigners();
  await mineUntilGasTarget(ethers.utils.parseUnits('5', 'gwei'));

  for (let i = startIndex; i <= maxIndex; i++) {
    let testAccountTargetIndex = i % numAccounts;
    let newOwner = accounts[testAccountTargetIndex].address;
    await giveMoonCatAway(i, newOwner, accounts[15]);
  }
}

async function disperseLootprints(startIndex, maxIndex, numAccounts = 5) {
  const accounts = await ethers.getSigners();
  const LOOTPRINTS = await MoonCatLootprints;
  await mineUntilGasTarget(ethers.utils.parseUnits('5', 'gwei'));

  let currentIndex = 0;
  for (let i = startIndex; i <= maxIndex; i++) {
    let details = await LOOTPRINTS.getDetails(i);
    if (details.status != 3) {
      //console.log(`Target lootprint ${i} doesn't exist...`);
      continue;
    }
    let testAccountTargetIndex = currentIndex % numAccounts;
    let newOwner = accounts[testAccountTargetIndex].address;
    await giveLootprintAway(i, newOwner, accounts[15]);
    currentIndex++;
  }
}

async function giveMoonCatAway(rescueOrder, newOwner, moneybag) {
  const mcr = await MoonCatRescue,
    oldWrapper = await OldWrappedMoonCats,
    acclimator = await MoonCatAcclimator;

  const transferCost = ethers.utils.parseEther('0.3')
  let catId = await mcr.rescueOrder(rescueOrder);
  let catData = await mcr.getCatDetails(catId);
  let owner = catData.owner;

  async function prepOldOwner(owner) {
    await network.provider.request({
      method: "hardhat_impersonateAccount",
      params: [owner]}
    );
    let signer = await ethers.provider.getSigner(owner);

    // Check if they have enough for the transfer
    let currentBalance = await signer.getBalance();
    if (currentBalance.lt(transferCost)) {
      await moneybag.sendTransaction({ to: owner, value: transferCost });
    }
    return signer;
  }

  if (owner == MOONCAT_WRAPPED_ADDRESS) {
    // This is an old-wrapped MoonCat; acclimate-and-transfer it to the new owner
    let wrappedId = await oldWrapper._catIDToTokenID(catId);
    owner = await oldWrapper.ownerOf(wrappedId);

    console.log(`Current owner ${owner} is giving wrapped ${wrappedId} ${catId} (#${rescueOrder}) => ${newOwner}`);
    let signer = await prepOldOwner(owner);

    let data = numToBytes32(rescueOrder) + newOwner.substr(2);
    await oldWrapper.connect(signer)['safeTransferFrom(address,address,uint256,bytes)'](
      owner, MOONCAT_ACCLIMATOR_ADDRESS, wrappedId, data
    );
  } else if (owner == MOONCAT_ACCLIMATOR_ADDRESS) {
    // Already acclimated
    owner = await acclimator.ownerOf(rescueOrder);
    if (owner == newOwner){
      console.log(`Target owner ${newOwner} already owns ${catId} (#${rescueOrder})...`);
      return;
    }

    // Send to new owner
    let signer = await prepOldOwner(owner);
    console.log(`Current owner ${owner} is giving acclimated ${catId} (#${rescueOrder}) => ${newOwner}`);
    await acclimator.connect(signer)['safeTransferFrom(address,address,uint256)'](
      owner, newOwner, rescueOrder
    );
  } else {
    // Original MoonCat; wrap and transfer it to new owner
    console.log(`Current owner ${owner} is giving ${catId} (#${rescueOrder}) => ${newOwner}`);
    let signer = await prepOldOwner(owner);
    await mcr.connect(signer).makeAdoptionOfferToAddress(catId, 0, MOONCAT_ACCLIMATOR_ADDRESS);
    await acclimator.connect(signer).wrap(rescueOrder);
    await acclimator.connect(signer)['safeTransferFrom(address,address,uint256)'](
      owner, newOwner, rescueOrder
    );
  }

  // Cleanup
  await network.provider.request({
    method: "hardhat_stopImpersonatingAccount",
    params: [owner]}
  );
}

async function giveLootprintAway(tokenId, newOwner, moneybag) {
  const LOOTPRINTS = await MoonCatLootprints;

  const transferCost = ethers.utils.parseEther('0.3')
  let owner = await LOOTPRINTS.ownerOf(tokenId);

  async function prepOldOwner(owner) {
    await network.provider.request({
      method: "hardhat_impersonateAccount",
      params: [owner]}
    );
    let signer = await ethers.provider.getSigner(owner);

    // Check if they have enough for the transfer
    let currentBalance = await signer.getBalance();
    if (currentBalance.lt(transferCost)) {
      await moneybag.sendTransaction({ to: owner, value: transferCost });
    }
    return signer;
  }

  if (owner == newOwner){
    console.log(`Target owner ${newOwner} already owns ${tokenId}...`);
    return;
  }

  // Send to new owner
  let signer = await prepOldOwner(owner);
  console.log(`Current owner ${owner} is giving lootprint ${tokenId} => ${newOwner}`);
  await LOOTPRINTS.connect(signer)['safeTransferFrom(address,address,uint256)'](
    owner, newOwner, tokenId
  );

  // Cleanup
  await network.provider.request({
    method: "hardhat_stopImpersonatingAccount",
    params: [owner]}
  );
}

/**
 * The ability to parse custom errors to get their arguments is being added in Waffle 4.0:
 * https://ethereum-waffle.readthedocs.io/en/latest/migration-guides.html#custom-errors
 * Until that's added, this custom parser can be used to check the properties
 */
async function expectPermissionError(tx, role, address) {
  try {
    await tx;
    expect.fail('Expected transaction to revert, but it succeeded');
  } catch (err) {
    let expectedMessage;
    if (err.message.indexOf('with custom error') > 0) {
      // Custom error was recognized
      expectedMessage = `VM Exception while processing transaction: reverted with custom error 'MissingRole("${role}", "${address}")'`;
    } else {
      // Raw error message
      let paddedRole = ('0000000000000000000000000000000000000000000000000000000000000000'+(role.substr(2).toLowerCase())).slice(-64);
      let paddedAddr = ('0000000000000000000000000000000000000000000000000000000000000000'+(address.substr(2).toLowerCase())).slice(-64);
      expectedMessage = `VM Exception while processing transaction: reverted with an unrecognized custom error (return data: 0x75000dc0${paddedRole}${paddedAddr})`
    }
    expect(err.message).to.equal(expectedMessage, "Missing Role error thrown");
  }
}

function numToBytes32(num) {
  return '0x' + num.toString(16).padStart(64, '0');
}


module.exports = {
  MoonCatRescue,
  OldWrappedMoonCats,
  MoonCatAcclimator,
  MoonCatAccessories,
  MoonCatLootprints,
  JumpPort,

  disperseMoonCats,
  giveMoonCatAway,
  disperseLootprints,
  giveLootprintAway,

  mineUntilGasTarget,
  expectPermissionError,
  numToBytes32
}
