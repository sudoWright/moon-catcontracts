This package is published on NPM as [`@mooncatrescue/contracts`](https://www.npmjs.com/package/@mooncatrescue/contracts). The bundle includes the Solidity interface files for MoonCat-related smart contracts and the built artifacts (containing their ABI info), plus the `moonCatUtils.js` utility script.

The folder `/contracts` is the core of what gets published; content that exists outside that folder (in the root of this repository) is used to build the bundle, but is not included in it.

To release a new version, ensure you're on the latest tip of the main branch, and then:

    npm login
    npx hardhat clean
    npx hardhat compile
    cp README.md contracts/
    cd contracts
    npm publish --access public
